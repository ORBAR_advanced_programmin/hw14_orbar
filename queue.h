#include <iostream>
#ifndef QUEUE_H
#define QUEUE_H


/* a queue contains positive integer values. */
typedef struct queue 
{
	int* _elements;
	int _maxSize;
	int _count;
	int _topIndex;
	
} queue;
/*
The function restarts the queue
input:pointer of queue and the size of the queue
output:none
*/
void initQueue(queue* q, unsigned int size);
/*
The function cleans the queue
input:
*/
void cleanQueue(queue* q);
/*
The function adds value to the queue
input:pointer of queue and new un nagative value to add
output:none
*/
void enqueue(queue* q, unsigned int newValue);
/*
the function returns element in top of queue, or -1 if empty
input:pointer of queue
output:element in top of queue, or -1 if empty
*/
int dequeue(queue* q); // return element in top of queue, or -1 if empty


#endif /* QUEUE_H */
