#include <iostream>
#ifndef UTILS_H
#define UTILS_H
/*
The function reverses an array
input:array and his size
output:none
*/
void reverse(int* nums, unsigned int size);
/*
The function gets 10 numbers from user into an array and reverses him and returns him
input:none
output:none
*/
int* reverse10();

#endif // UTILS_H