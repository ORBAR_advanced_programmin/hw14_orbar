#include <iostream>
#ifndef STACK_H
#define STACK_H

/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	struct list *next;
} stack;
/*
The function pushes a value into the stack
input:pointer to stack,un nagative value to push
output:none
*/
void push(stack** s, unsigned int element);
/*
The function pops put a value from stack
input:pointer to stack
output:the value that got poped or -1 if the stack is empty
*/
int pop(stack** s); 
/*
The function restart the stack
input:pointer to stack
output:none
*/
void initStack(stack** s);
/*
The function cleans the stack
input:pointer to stack
output:none
*/
void cleanStack(stack** s);

#endif // STACK_H
