#include <iostream>
struct list
{
	
	unsigned int _n;
	struct list* next;
};
typedef struct list list;
/*
the function removes the the value from the end of the list
input:address of list pointer
output:the value that got deleted(from the end of the list)
*/
int remove(list** head);
/*
the function creats a new list and returns him
input:un nagative number
output:a new list with the number
*/
list* createList(unsigned int n);
/*
insert a value to the end of the list
input:a address of list pointer,un nagative number
output:none
*/
void insertAtEnd(list** head, unsigned int element);
