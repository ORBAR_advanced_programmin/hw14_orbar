#include <iostream>
#include "utlis.h"
#include "stack.h"
#include "linkedList.h"
/*
The function reverses an array
input:array and his size
output:none
*/
void reverse(int* nums, unsigned int size)
{
	int i = 0;
	stack* s = NULL;
	initStack(&s);
	for (i; i < size; i++)
	{
		push(&s, nums[i]);
	}
	i = 0;
	for (i; i < size; i++)
	{
		nums[i] = pop(&s);
	}
}
/*
The function gets 10 numbers from user into an array and reverses him and returns him
input:none
output:none
*/
int* reverse10()
{
	int i = 0;
	int* arr = new int[10];
	for (i; i < 10; i++)
	{
		std::cout << "Enter number " << i + 1 << ": ";
		std::cin >> arr[i];
	}
	reverse(arr, 10);
	return arr;

}