#include <iostream>
#include "queue.h"
/*
The function restarts the queue
input:pointer of queue and the size of the queue
output:none
*/
void initQueue(queue* q, unsigned int size)
{
	q->_count = 0;
	q->_maxSize = size;
	q->_elements = new unsigned int[size];
	q->_topIndex = 0;
	
}
/*
The function cleans the queue
input:
*/
void cleanQueue(queue* q)
{
	delete[] q->_elements;
}
/*
The function adds value to the queue
input:pointer of queue and new un nagative value to add
output:none
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q->_count == 0)
	{
		q->_topIndex = 0;
	}
	if (!(q->_count == q->_maxSize))//if stack isnt full
	{
		q->_elements[q->_topIndex+q->_count] = newValue;	
		q->_count++;
	}
}
/*
the function returns element in top of queue, or -1 if empty
input:pointer of queue
output:element in top of queue, or -1 if empty
*/
int dequeue(queue* q)
{
	int ans = -1;
	if (!(q->_count == 0))//if stack isnt empty
	{
		q->_count--;
		ans = q->_elements[q->_topIndex];
		q->_topIndex++;	
	}
	
	return ans;
	
}