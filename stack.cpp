#include <iostream>
#include "stack.h"
#include "linkedList.h"

/*
The function pushes a value into the stack
input:address to stack,un nagative value to push. For example(important): stack s; push(&s,6)
output:none
*/
void push(stack** s, unsigned int element)
{
	insertAtEnd(&(*s)->next, element);
}
/*
The function pops put a value from stack
input:pointer to stack
output:the value that got poped or -1 if the stack is empty
*/
int pop(stack** s)
{
	return(remove(&(*s)->next));
}
 /*
 The function restart the stack
 input:pointer to stack
 output:none
 */
void initStack(stack** s)
{
	*s = new stack[sizeof(stack)];
	(*s)->next = NULL;
}
/*
The function cleans the stack
input:pointer to stack
output:none
*/
void cleanStack(stack** s)
{
	delete *s;
	initStack(s);
}