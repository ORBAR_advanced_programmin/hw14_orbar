#include <iostream>
#include "linkedList.h"
/*
the function removes the the value from the end of the list
input:address of list pointer
output:the value that got deleted(from the end of the list) or -1 if the list if empty
*/
int remove(list** head)
{
	list* curr = *head;
	list* temp = NULL;
	int deletedValue = 0;
	// if the list is not empty (if list is empty - nothing to delete!)
	if (*head)
	{
		if ((*head)->next == NULL)//first node should be deleted?
		{
			deletedValue = curr->_n;
			*head = (*head)->next;	
			delete curr;
			return deletedValue;
		}
		else
		{
			while (curr)
			{
				if (curr->next->next == NULL) // Last node
				{
					deletedValue = curr->next->_n;
					delete(curr->next);
					curr->next = NULL;
					return deletedValue;
					
				}
				else
				{
					curr = curr->next;
				}
			}
		}
	}
	else
	{
		return -1;
	}
	
}
/*
the function creats a new list and returns him
input:un nagative number
output:a new list with the number
*/
list* createList(unsigned int n)
{
	list* newList = new list[sizeof(list)];
	newList->_n = n;
	newList->next = NULL;
	return newList;

}
/*
insert a value to the end of the list
input:a address of list pointer,un nagative number
output:none
*/
void insertAtEnd(list** head, unsigned int element)
{
	list* newNode = createList(element);
	list* curr = *head;
	if (!*head) // empty list!
	{
		*head = newNode;
	}
	else
	{
		while (curr->next) // while the next is NOT NULL (when next is NULL - that is the last node)
		{
			curr = curr->next;
		}

		curr->next = newNode;
		newNode->next = NULL;
	}
}